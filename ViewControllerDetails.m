//
//  ViewControllerDetails.m
//  Matapi
//
//  Created by Dan Lakss on 2015-05-11.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "ViewControllerDetails.h"

@interface ViewControllerDetails ()
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (nonatomic) NSDictionary *nutrientsList;
@property (nonatomic) int *healthiness;
@property (weak, nonatomic) IBOutlet UILabel *label_itemName;
@property (weak, nonatomic) IBOutlet UILabel *label_energy;
@property (weak, nonatomic) IBOutlet UILabel *label_fat;
@property (weak, nonatomic) IBOutlet UILabel *label_protein;
@property (weak, nonatomic) IBOutlet UILabel *label_healthiness;

//Dynamics
@property (nonatomic) UIDynamicAnimator *animator; //Animator controlling behaviour
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;

@end

@implementation ViewControllerDetails

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
	//Load nutrientsList with nutrients
	self.nutrientsList = [[NSDictionary alloc]init];
	self.nutrientsList = [self.data objectForKey:@"nutrientValues"];
    

	self.healthiness += [[self.nutrientsList objectForKey:@"fat"] intValue] + [[self.nutrientsList objectForKey:@"energyKcal"] intValue];
    NSString *healthy = [NSString stringWithFormat:@"%i", self.healthiness];
	
	
	//Set labels
	self.label_itemName.text = [self.data objectForKey:@"name"];
	self.label_fat.text = [self.label_fat.text stringByAppendingString:[[self.nutrientsList objectForKey:@"fat"] stringValue]];
	
	self.label_energy.text = [self.label_energy.text stringByAppendingString:[[self.nutrientsList objectForKey:@"energyKcal"] stringValue]];
	
	self.label_protein.text = [self.label_protein.text stringByAppendingString:[[self.nutrientsList objectForKey:@"protein"] stringValue]];
    
    self.label_healthiness.text = [self.label_healthiness.text stringByAppendingString:healthy];
    
	
	

    
    //Dynamics
    self.animator = [[UIDynamicAnimator alloc]initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc]initWithItems:@[self.label_fat]];
    [self.animator addBehavior:self.gravity];
    self.collision = [[UICollisionBehavior alloc ]initWithItems:@[self.label_fat]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior:self.collision];
	
	
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
