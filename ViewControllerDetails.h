//
//  ViewControllerDetails.h
//  Matapi
//
//  Created by Dan Lakss on 2015-05-11.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewControllerDetails : UIViewController
@property (nonatomic) NSDictionary *data;
@property (nonatomic) NSInteger indexId;
@end
