//
//  ItemsTableViewController.m
//  Matapi
//
//  Created by Dan Lakss on 2015-02-22.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "ItemsTableViewController.h"
#import "ViewControllerDetails.h"

@interface ItemsTableViewController ()
@property (strong, nonatomic) IBOutlet UITableView *itemsTableView;
@property (nonatomic) ViewControllerDetails *destinationController;
@property (nonatomic) NSDictionary *itemData;
@property (nonatomic) NSInteger indexId;


@end

@implementation ItemsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	for(int i = 0 ; i < [self.dataList count];i++){
		NSLog(@"Data from tableView with item: %@", [self.dataList objectAtIndex:i]);
		
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	//Actions based on item selection
	//Pass item from search results
	
	self.destinationController = [[ViewControllerDetails alloc]init];
	//Perform new http request based on respective items number
	
	//HTTP request
	NSString *searchString = [NSString stringWithFormat:@"http://www.matapi.se/foodstuff/%@", [[self.dataList objectAtIndex:indexPath.row] objectForKey:@"number"]];
	NSURL *url = [NSURL URLWithString:searchString];
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	NSURLSession *session = [NSURLSession sharedSession];
	NSLog(@"Item in table view clicked!");
	
	NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
		NSError *parseError;
		NSDictionary *dataList = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
		
		dispatch_async(dispatch_get_main_queue(), ^{
			self.itemData = dataList;
			self.indexId = indexPath.row;
			//Create segue
			[self performSegueWithIdentifier:@"searchDetailsSegue" sender:self];
			
			
		});
		
	}];
	
	[task resume];
	
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return [self.dataList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.textLabel.text = [[self.dataList objectAtIndex:indexPath.row]objectForKey:@"name"];
	    return cell;
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
	NSLog(@"Search results to details segue works!");
	if([[segue identifier]isEqualToString:@"searchDetailsSegue"]){
		self.destinationController = [segue destinationViewController];
		self.destinationController.data = self.itemData;
		self.destinationController.indexId = self.indexId;
	}

	
}




@end
