//
//  ItemsTableViewController.h
//  Matapi
//
//  Created by Dan Lakss on 2015-02-22.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemsTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic) NSArray *dataList;
@end
