//
//  ViewController.m
//  Matapi
//
//  Created by Dan Lakss on 2015-02-22.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic) NSMutableArray *fruitList;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic) NSArray *data;
@property (nonatomic) ItemsTableViewController *tableViewController;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;




@end

@implementation ViewController
- (IBAction)buttonVegetables:(UIButton *)sender {
    NSLog(@"Vegetables pressed");
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
	//Create segue to ItemsTableViewController & pass data
	self.tableViewController = [[ItemsTableViewController alloc]init];
	
	//HTTP request
	NSString *searchString = [NSString stringWithFormat:@"http://www.matapi.se/foodstuff?query=%@", searchBar.text];
	NSURL *url = [NSURL URLWithString:searchString];
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	NSURLSession *session = [NSURLSession sharedSession];
	NSLog(@"Search button clicked!");
	
	NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
		NSError *parseError;
		NSArray *dataList = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
		
		dispatch_async(dispatch_get_main_queue(), ^{
			self.data = dataList;

			//Create segue
			[self performSegueWithIdentifier:@"searchResultsSegue" sender:self];
			
			
		});
		
	}];
	
	[task resume];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
	if([[segue identifier] isEqualToString:@"searchResultsSegue"]){
		self.tableViewController = [segue destinationViewController];
		self.tableViewController.dataList = self.data;
	}

	

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [UIView beginAnimations:nil context:nil];
    
    self.labelTitle.center = CGPointMake(self.view.frame.size.width - self.labelTitle.frame.size.width/2, self.labelTitle.frame.origin.y);
    [UIView setAnimationDuration:20.0];
    [UIView commitAnimations];
	
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
