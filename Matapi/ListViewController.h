//
//  ListViewController.h
//  Matapi
//
//  Created by Dan Lakss on 2015-02-22.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListViewController : UITableViewController 
    //Hooked up to the table view as a data source in the List scene
    

@end
